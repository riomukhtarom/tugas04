import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;
public class CopyPaste {
	public static void main(String[] args){
		String copyFile = "copyFile.txt";
		String pasteFile = "pasteCopyFile.txt";
		
		try{
			String contentCopyFile = null;
			FileReader fileReader = new FileReader(copyFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			
			String contentPasteFile = null;
			File file = new File(pasteFile);
			FileWriter fileWriter = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			
			while((contentCopyFile = bufferedReader.readLine()) != null){
				contentPasteFile = contentCopyFile;
				if(!file.exists()){
					file.createNewFile();
				}
				bufferedWriter.write(contentPasteFile);
				bufferedWriter.newLine();
				System.out.println(contentPasteFile);                
			}
			bufferedReader.close();
			bufferedWriter.close();
		}
		catch(FileNotFoundException e){
			System.out.println("File "+copyFile+" tidak ditemukan");
		}
		catch(IOException e){
			System.out.println("File "+copyFile+" tidak dapat dibaca");
		}
		catch(Exception e){
			System.out.println(e);
		}
	}
}
