import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
public class PascalTriangle {
    public static void main(String[] args) {
        System.out.println("Pascal Triangle \n");
        try{
            String fileHigh = "fileHigh.txt";
            FileReader fileReader = new FileReader(fileHigh);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String high = null;
            high = bufferedReader.readLine();
            int highTriangle = Integer.parseInt(high);
            
            String resultPascalTriangle = "resultPascalTriangle.txt";
            File fileResult = new File(resultPascalTriangle);
            FileWriter fileWriter = new FileWriter(fileResult.getAbsoluteFile());
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            
            for (int indeks1=0; indeks1<highTriangle; indeks1++){
                for (int indeks2=1; indeks2<=(highTriangle-indeks1)*2; indeks2++){
                        System.out.print(" ");
                        bufferedWriter.write(" ");
                }
                int number = 1;
                for (int indeks3=0; indeks3<=indeks1; indeks3++){
                        System.out.print(number+"   ");
                        bufferedWriter.write(number+"   ");
                        number = number * (indeks1-indeks3)/(indeks3+1);
                }
                System.out.println();
                bufferedWriter.newLine();
            }
            bufferedReader.close();
            bufferedWriter.close();
        }
        catch(FileNotFoundException e){
            System.out.println("File Tidak Ditemukan");
        }
        catch(IOException e){
            System.out.println("File Tidak Dapat Dibaca");
        }
    }
}
