import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.File;
public class PermutasiCombinasi{
    public static int permutasi(int n, int r){
        int hasil_n = 1;
        for(int indeks1=n; indeks1>1; indeks1--){
            hasil_n = hasil_n * indeks1;
        }
        int hasil_nr = 1;
        for(int indeks2=(n-r); indeks2>1; indeks2--){
            hasil_nr = hasil_nr * indeks2;
        }
        return(hasil_n/hasil_nr);
    }
    public static int combinasi(int n, int r){
        int hasil_n = 1;
        for(int indeks1=n; indeks1>=1; indeks1--){
            hasil_n = hasil_n * indeks1;
        }
        int hasil_r = 1;
        for(int indeks2=r; indeks2>=1; indeks2--){
            hasil_r = hasil_r * indeks2;
        }
        int hasil_nr = 1;
        for(int indeks3=(n-r); indeks3>=1; indeks3--){
            hasil_nr = hasil_nr * indeks3;
        }
        return(hasil_n/(hasil_r * hasil_nr));
    }
    public static void main(String[] args){
        InputStreamReader inputStreamReader = new InputStreamReader(System.in);
        BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
        int n1, r1, choice, n2, r2, ulang = 0;
        String inputan;
        try{
            System.out.println("Masukkan pilihan Anda : ");
            System.out.println("1. Permutasi");
            System.out.println("2. Combinasi");
            System.out.println("0. Keluar");
            System.out.print("=>");
            inputan = input.readLine();

            switch(choice = Integer.parseInt(inputan)){
                case 1:
                    String sourcePermutasi = "sourcePermutasi.txt";
                    FileReader fileReaderPermutasi = new FileReader(sourcePermutasi);
                    BufferedReader bufferedReaderPermutasi = new BufferedReader(fileReaderPermutasi);
                    
                    String resultPermutasi = "resultPermutasi.txt";
                    File fileResultPermutasi = new File(resultPermutasi);
                    FileWriter fileWriterPermutasi = new FileWriter(fileResultPermutasi.getAbsoluteFile());
                    BufferedWriter bufferedWriterPermutasi = new BufferedWriter(fileWriterPermutasi);
                    
                    String contentSourcePermutasi = null;
                    int[] linePermutasi = new int[2];
                    byte indeksPermutasi = 0;
                    while(indeksPermutasi < 2){
						contentSourcePermutasi = bufferedReaderPermutasi.readLine();
                        linePermutasi[indeksPermutasi] = Integer.parseInt(contentSourcePermutasi);
                        indeksPermutasi ++;
                    }
                    bufferedWriterPermutasi.write("Nilai Variabel N : "+linePermutasi[0]);
					bufferedWriterPermutasi.newLine();
                    bufferedWriterPermutasi.write("Nilai Variabel R : "+linePermutasi[1]);
                    bufferedWriterPermutasi.newLine();
					System.out.println("Nilai Variabel N : "+linePermutasi[0]);
                    System.out.println("Nilai Variabel R : "+linePermutasi[1]);
                    if(linePermutasi[0]>0 && linePermutasi[1]>0 && linePermutasi[0]>linePermutasi[1]){
                        bufferedWriterPermutasi.write("Permutasinya "+permutasi(linePermutasi[0],linePermutasi[1]));
                        System.out.println("Permutasinya "+permutasi(linePermutasi[0],linePermutasi[1]));
                    }
                    else{
                        bufferedWriterPermutasi.write("Nilai N harus lebih besar dari R");
                        System.out.println("Nilai N harus lebih besar dari R");
                    }
					bufferedReaderPermutasi.close();
					bufferedWriterPermutasi.close();
                    break;
					
                case 2:
                    String sourceCombinasi = "sourceCombinasi.txt";
                    FileReader fileReaderCombinasi = new FileReader(sourceCombinasi);
                    BufferedReader bufferedReaderCombinasi = new BufferedReader(fileReaderCombinasi);
                    
                    String resultCombinasi = "resultCombinasi.txt";
                    File fileResultCombinasi = new File(resultCombinasi);
                    FileWriter fileWriterCombinasi = new FileWriter(fileResultCombinasi);
                    BufferedWriter bufferedWriterCombinasi = new BufferedWriter(fileWriterCombinasi);
                    
                    String contentSourceCombinasi = null;
                    int[] lineCombinasi = new int[2];
                    byte indeksCombinasi = 0;
                    while(indeksCombinasi < 2){
						contentSourceCombinasi = bufferedReaderCombinasi.readLine();
                        lineCombinasi[indeksCombinasi] = Integer.parseInt(contentSourceCombinasi);
                        indeksCombinasi ++;
                    }
                    bufferedWriterCombinasi.write("Nilai Variabel N : "+lineCombinasi[0]);
					bufferedWriterCombinasi.newLine();
                    bufferedWriterCombinasi.write("Nilai Variabel R : "+lineCombinasi[1]);
					bufferedWriterCombinasi.newLine();
                    System.out.println("Nilai Variabel N : "+lineCombinasi[0]);
                    System.out.println("Nilai Variabel R : "+lineCombinasi[1]);
                    if(lineCombinasi[0]>0 && lineCombinasi[1]>0 && lineCombinasi[0]>lineCombinasi[1]){
                        bufferedWriterCombinasi.write("Permutasinya "+permutasi(lineCombinasi[0],lineCombinasi[1]));
                        System.out.println("Permutasinya "+permutasi(lineCombinasi[0],lineCombinasi[1]));
                    }
                    else{
                        bufferedWriterCombinasi.write("Nilai N harus lebih besar dari R");
                        System.out.println("Nilai N harus lebih besar dari R");
                    }
					bufferedReaderCombinasi.close();
					bufferedWriterCombinasi.close();
                    break;
                case 0:
                    ulang = 0;
                    break;
                default:
                    System.out.println("Masukkan angka antara 0-2");
                    ulang = 1;
                    break;
            }
        }
        catch(Exception e){

        }
    }
}