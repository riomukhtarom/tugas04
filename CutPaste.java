import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.File;
public class CutPaste {
	public static void main(String[] args){
		String cutFile = "cutFile.txt";
		String pasteFile = "pasteCutFile.txt";
		
		try{
			String contentCutFile = null;
			FileReader fileReader = new FileReader(cutFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			
			String contentPasteFile = null;
			File filePaste = new File(pasteFile);
			FileWriter fileWriterPaste = new FileWriter(filePaste.getAbsoluteFile());
			BufferedWriter bufferedWriterPaste = new BufferedWriter(fileWriterPaste);
			
			while((contentCutFile = bufferedReader.readLine()) != null){
				contentPasteFile = contentCutFile;
				if(!filePaste.exists()){
					filePaste.createNewFile();
				}
				bufferedWriterPaste.write(contentPasteFile);
				bufferedWriterPaste.newLine();
				System.out.println(contentPasteFile);                
			}
			
			File fileCut = new File(cutFile);
			FileWriter fileWriterCut = new FileWriter(fileCut.getAbsoluteFile());
			BufferedWriter bufferedWriterCut = new BufferedWriter(fileWriterCut);
			contentCutFile = "";
			bufferedWriterCut.write(contentCutFile);
			
			bufferedWriterCut.close();
			bufferedReader.close();
			bufferedWriterPaste.close();
		}
		catch(FileNotFoundException e){
			System.out.println("File "+cutFile+" tidak ditemukan");
		}
		catch(IOException e){
			System.out.println("File "+cutFile+" tidak dapat dibaca");
		}
		catch(Exception e){
			System.out.println(e);
		}
	}
}
